<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover table-xs threats">
<thead class="table-xs">
<tr>
<th>Dreiging</th>
<th>Kans</th>
<th>Impact</th>
<th>Aanpak</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="threats/threat">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{/output/content/case/@id}/{@id}'">
<td><span class="table-xs">Dreiging</span><xsl:value-of select="threat" /></td>
<td><span class="table-xs">Kans</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Impact</span><xsl:value-of select="impact" /></td>
<td><span class="table-xs">Aanpak</span><xsl:value-of select="handle" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/{/output/content/case/@id}/template" class="btn btn-default">Nieuwe dreiging vanuit template</a>
<a href="/{/output/page}/{/output/content/case/@id}/nieuw" class="btn btn-default">Nieuwe dreiging</a>
</div>

<xsl:if test="count(threats/threat)>0">
<div class="btn-group right">
<a href="/casus/maatregelen/{../case/@id}" class="btn btn-default">Naar maatregelen</a>
</div>
</xsl:if>

<div id="help">
<p>In dit onderdeel geeft u de dreigingen op die gelden voor de systemen binnen de scope. U kunt gebruik maken van de dreigingen-templates uit RAVIB of zelf een dreiging toevoegen. De dreigingen-templates bevatten, naast een omschrijving, verwijzingen naar maatregelen uit de voor deze risicoanalyse gekozen maatregelenstandaard. Deze selectie kunt u in het volgende onderdeel aanpassen.</p>
</div>
</xsl:template>

<!--
//
//  Template template
//
//-->
<xsl:template match="templates">
<xsl:call-template name="show_messages" />
<p>Kies een van onderstaande generieke dreigingen als template voor de nieuwe dreiging.</p>

<table class="table table-condensed table-hover table-xs templates">
<thead>
<tr>
<th class="number">#</th>
<th class="threat">Dreiging-template</th>
<th class="cia">B</th>
<th class="cia">I</th>
<th class="cia">V</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="template">
<xsl:if test="category">
<tr class="category">
<td colspan="2"><xsl:value-of select="category" /></td>
<td>B</td>
<td>I</td>
<td>V</td>
</tr>
</xsl:if>
<tr onClick="javascript:document.location='/{/output/page}/{../../case/@id}/nieuw/{@id}'">
<td><xsl:value-of select="number" /></td>
<td><div class="threat"><xsl:value-of select="threat" /></div>
<div class="description"><xsl:value-of select="description" /></div></td>
<td><xsl:value-of select="availability" /></td>
<td><xsl:value-of select="integrity" /></td>
<td><xsl:value-of select="confidentiality" /></td>
</tr>
</xsl:for-each>

</tbody>
</table>

<div class="btn-group">
<a href="/{/output/page}/{../case/@id}/nieuw" class="btn btn-default">Geen template gebruiken</a>
<a href="/{/output/page}/{/output/content/case/@id}" class="btn btn-default">Afbreken</a>
</div>

<div id="help">
<p>In de kolom B, I en V kan een p of een s staan. De p staat voor primair en de s voor secundair. Dit geeft aan of een dreiging primair of secundair een gevaar vormt voor de beschikbaarheid, integriteit of vertrouwelijkheid.</p>
</div>
</xsl:template>

<!--
//
//  Measures template
//
//-->
<xsl:template match="measures">
<table class="table table-condensed">
<thead>
<tr><th>#</th><th>Maatregel uit <xsl:value-of select="standard" /></th></tr>
</thead>
<tbody>
<xsl:for-each select="item">
<tr>
<td><xsl:value-of select="number" /></td>
<td><xsl:value-of select="name" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />
<form action="/{/output/page}/{/output/content/case/@id}/{threat/template_id}" method="post">
<xsl:if test="threat/@id">
<input type="hidden" name="id" value="{threat/@id}" />
</xsl:if>
<xsl:if test="threat/template_id">
<input type="hidden" name="template_id" value="{threat/template_id}" />
</xsl:if>

<xsl:if test="threat/template">
<h2><xsl:value-of select="threat/template" /></h2>
<div class="description"><xsl:value-of select="threat/description" /></div>
<div class="row form-group">
<div class="col-sm-4">Beschikbaarheid: <xsl:value-of select="threat/availability" /></div>
<div class="col-sm-4">Integriteit: <xsl:value-of select="threat/integrity" /></div>
<div class="col-sm-4">Vertrouwelijkheid: <xsl:value-of select="threat/confidentiality" /></div>
</div>
</xsl:if>
<label for="threat">Dreiging:</label>
<input type="text" id="theat" name="threat" value="{threat/threat}" class="form-control" />
<label for="actor">Meest bedreigende actor:</label>
<select id="actor" name="actor_id" class="form-control">
<xsl:for-each select="actors/actor">
<option value="{@id}"><xsl:if test="@id=../../threat/actor_id"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
<div class="row">
<div class="col-sm-4">
<label for="chance">Kans:</label>
<select id="chance" name="chance" class="form-control">
<xsl:for-each select="chance/option">
<option value="{@value}"><xsl:if test="@value=../../threat/chance"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="col-sm-4">
<label for="impact">Impact:</label>
<select id="impact" name="impact" class="form-control">
<xsl:for-each select="impact/option">
<option value="{@value}"><xsl:if test="@value=../../threat/impact"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
<div class="col-sm-4">
<label for="handle">Aanpak:</label>
<select id="handle" name="handle" class="form-control">
<xsl:for-each select="handle/option">
<option value="{@value}"><xsl:if test="@value=../../threat/handle"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if><xsl:value-of select="." /></option>
</xsl:for-each>
</select>
</div>
</div>
<label for="argumentation">Argumentatie voor gemaakte keuze:</label>
<textarea id="argumentation" name="argumentation" class="form-control"><xsl:value-of select="threat/argumentation" /></textarea>

<h2>Geraakte systemen</h2>
<table class="table table-condensed table-striped table-xs scope">
<thead class="table-xs">
<tr>
<th></th>
<th>Informatiesysteem</th>
<th>Beschikbaarheid</th>
<th>Integriteit</th>
<th>Vertrouwelijkheid</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="scope/system">
<tr>
<td><span class="table-xs">Geraakt</span><input type="checkbox" name="threat_scope[]" value="{@id}"><xsl:if test="selected='yes'"><xsl:attribute name="checked">checked</xsl:attribute></xsl:if></input></td>
<td><span class="table-xs">Informatiesysteem</span><xsl:value-of select="item" /></td>
<td><span class="table-xs">Beschikbaarheid</span><xsl:value-of select="availability" /></td>
<td><span class="table-xs">Integriteit</span><xsl:value-of select="integrity" /></td>
<td><span class="table-xs">Vertrouwelijkheid</span><xsl:value-of select="confidentiality" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<h2>Maatregelen</h2>
<div class="row">
<div class="col-sm-6">
<label for="curent">Huidige situatie:</label>
<textarea id="current" name="current" class="form-control"><xsl:value-of select="threat/current" /></textarea>
</div>
<div class="col-sm-6">
<label for="action">Gewenste situatie:</label>
<textarea id="action" name="action" class="form-control"><xsl:value-of select="threat/action" /></textarea>
</div>
</div>
<xsl:apply-templates select="threat/measures" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Dreiging opslaan" class="btn btn-default" />
<a href="/{/output/page}/{/output/content/case/@id}" class="btn btn-default">Afbreken</a>
<xsl:if test="threat/@id">
<input type="submit" name="submit_button" value="Dreiging verwijderen" class="btn btn-default" onClick="javascript:return confirm('VERWIJDEREN: Weet u het zeker?')" />
</xsl:if>
</div>
<div class="btn-group">
<a href="/casus/risico" target="_blank" class="btn btn-default">Risio-inschatting</a>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Dreigingen</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="templates" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
