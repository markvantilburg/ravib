<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<table class="table table-condensed table-striped table-hover">
<thead>
<tr>
<th>Scenario</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="scenarios/scenario">
<tr class="click" onClick="javascript:document.location='/{/output/page}/{../../../case/@id}/{@id}'">
<td><xsl:value-of select="title" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left">
<a href="/{/output/page}/{../case/@id}/nieuw" class="btn btn-default">Nieuw scenario</a>
</div>
<div class="btn-group right">
<a href="/casus/rapportage/{../case/@id}" class="btn btn-default">Naar rapportage</a>
</div>

<div id="help">
<p>Bedenk hoe meerdere dreigingen samen kunnen leiden tot een organisatie-ontwrichtend incident, waarbij kleine en/of eenvoudige dreigingen dienen als opstap naar een dreiging met een grote impact.</p>
</div>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<xsl:call-template name="show_messages" />

<!-- Actors -->
<h2>Actoren<span class="show_table" onClick="javascript:$('table.actors').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs actors">
<thead class="table-xs">
<tr><th>Actor</th><th>Bereidheid / kans</th><th>Kennisniveau</th><th>Middelen</th><th>Dreiging</th></tr>
</thead>
<tbody>
<xsl:for-each select="actors/actor">
<tr>
<td><span class="table-xs">Actor</span><xsl:value-of select="name" /></td>
<td><span class="table-xs">Bereidheid / kans</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Kennisniveau</span><xsl:value-of select="knowledge" /></td>
<td><span class="table-xs">Middelen</span><xsl:value-of select="resources" /></td>
<td><span class="table-xs">Dreiging</span><xsl:value-of select="threat" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<!-- Threats -->
<h2>Dreigingen<span class="show_table" onClick="javascript:$('table.threats').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs threats">
<thead class="table-xs">
<tr>
<th>Dreiging</th>
<th>Kans</th>
<th>Impact</th>
<th>Urgentie</th>
<th>Aanpak</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="threats/threat">
<tr>
<td><span class="table-xs">Dreiging</span><xsl:value-of select="threat" /></td>
<td><span class="table-xs">Kans</span><xsl:value-of select="chance" /></td>
<td><span class="table-xs">Impact</span><xsl:value-of select="impact" /></td>
<td><span class="table-xs">Urgentie</span><span class="urgency{risk_value}"><xsl:value-of select="risk_label" /></span></td>
<td><span class="table-xs">Aanpak</span><xsl:value-of select="handle" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<!-- BIA -->
<h2>BIA<span class="show_table" onClick="javascript:$('table.bia').toggle()">+</span></h2>
<table class="table table-condensed table-striped table-xs bia">
<thead class="table-xs">
<tr><th>Systeem</th><th>Impact van incident</th></tr>
</thead>
<tbody>
<xsl:for-each select="bia/item">
<tr>
<td><span class="table-xs">Systeem</span><xsl:value-of select="item" /></td>
<td><span class="table-xs">Impact van incident</span><xsl:value-of disable-output-escaping="yes" select="impact" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<form action="/{/output/page}/{../case/@id}" method="post">
<xsl:if test="scenario/@id">
<input type="hidden" name="id" value="{scenario/@id}" />
</xsl:if>

<label for="title">Titel:</label>
<input type="text" id="title" name="title" value="{scenario/title}" class="form-control" />
<label for="scenario">Scenario:</label>
<textarea id="scenario" name="scenario" class="form-control"><xsl:value-of select="scenario/scenario" /></textarea>
<label for="consequences">Mogelijke gevolgen:</label>
<textarea id="consequences" name="consequences" class="form-control"><xsl:value-of select="scenario/consequences" /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Scenario opslaan" class="btn btn-default" />
<a href="/{/output/page}/{../case/@id}" class="btn btn-default">Afbreken</a>
<xsl:if test="scenario/@id">
<input type="submit" name="submit_button" value="Scenario verwijderen" class="btn btn-default" onClick="javascript:return confirm('VERWIJDEREN: Weet u het zeker?')" />
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Scenario's</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
