<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}/{../case/@id}" method="post">
<table class="table table-condensed table-striped table-xs scope">
<thead class="table-xs">
<tr>
<th>Informatiesysteem</th>
<th>Waarde</th>
<th>Eigenaar</th>
<th>Persoonsgegevens</th>
<th>Lokatie</th>
<th class="visible last"></th>
<th class="hidden last">Scope</th>
</tr>
</thead>
<tbody>
<xsl:for-each select="item[scope='yes']">
<tr>
<td><span class="table-xs">Informatiesysteem</span><xsl:value-of select="item" /></td>
<td><span class="table-xs">Waarde</span><xsl:value-of select="value" /></td>
<td><span class="table-xs">Eigenaar</span><xsl:value-of select="owner" /></td>
<td><span class="table-xs">Persoonsgegevens</span><xsl:value-of select="personal_data" /></td>
<td><span class="table-xs">Lokatie</span><xsl:value-of select="location" /></td>
<td class="visible"></td>
<td class="hidden"><input type="checkbox" name="scope[]" value="{@id}" checked="checked" /></td>
</tr>
</xsl:for-each>
<xsl:for-each select="item[scope='no']">
<tr class="hidden">
<td><xsl:value-of select="item" /></td>
<td><xsl:value-of select="value" /></td>
<td><xsl:value-of select="owner" /></td>
<td><xsl:value-of select="personal_data" /></td>
<td><xsl:value-of select="location" /></td>
<td class="visible"></td>
<td><input type="checkbox" name="scope[]" value="{@id}" /></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group left visible">
<input type="button" value="Aanpassen" class="btn btn-default" onClick="javascript:enable_form()" />
</div>
<div class="btn-group left hidden">
<input type="submit" name="submit_button" value="Opslaan" class="btn btn-default" />
<input type="button" value="Afbreken" class="btn btn-default" onClick="javascript:disable_form()" />
</div>
</form>

<xsl:if test="count(item[scope='yes'])>0">
<div class="btn-group right visible">
<a href="/casus/belangen/{../case/@id}" class="btn btn-default">Naar belangen</a>
</div>
</xsl:if>

<div id="help">
<p>Selecteer systemen die onderdeel zijn van deze risicoanalyse. De systemen in de lijst zijn de systemen die in het BIA onderdeel zijn toegevoegd.</p>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Scope</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
