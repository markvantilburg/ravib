<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<form action="/{/output/page}/{../case/@id}" method="post">
<div class="row">
<div class="col-sm-6">
<div><input type="checkbox" name="extra_info" />Extra uitleg toevoegen.</div>
<div><input type="checkbox" name="accepted_risks" />Geaccepteerde dreigingen opnemen.</div>
<div><input type="checkbox" name="sort_by_risk" checked="checked" />Sorteren op risico / urgentie.</div>
</div>
<div class="col-sm-6 backup alert alert-warning">
Vergeet niet om je risicoanalyses veilig te stellen, door via <a href="/data">databeheer</a> een export te maken en die goed te bewaren!
</div>
</div>

<div class="btn-group left">
<input type="submit" name="submit_button" value="Genereer rapportage" class="btn btn-default" />
</div>
</form>

<div class="btn-group right">
<a href="/casus/voortgang/{../case/@id}" class="btn btn-default">Naar voortgang</a>
</div>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Rapportage</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
