<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="../banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<xsl:call-template name="show_messages" />
<div class="interests"><xsl:value-of select="." /></div>

<div class="btn-group left">
<a href="/{/output/page}/{../case/@id}/edit" class="btn btn-default">Aanpassen</a>
<a href="/{/output/page}/{../case/@id}/handout" class="btn btn-default">Handout maken</a>
</div>
<xsl:if test=".!=''">
<div class="btn-group right">
<a href="/casus/dreigingen/{../case/@id}" class="btn btn-default">Naar dreigingen</a>
</div>
</xsl:if>
</xsl:template>

<!--
//
//  Edit template
//
//-->
<xsl:template match="edit">
<form action="/{/output/page}/{../case/@id}" method="post">
<textarea name="interests" class="form-control"><xsl:value-of select="." /></textarea>

<div class="btn-group">
<input type="submit" name="submit_button" value="Opslaan" class="btn btn-default" />
<xsl:if test=".!=''">
<a href="/{/output/page}/{../case/@id}" class="btn btn-default">Afbreken</a>
</xsl:if>
</div>
</form>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="breadcrumbs" />
<h1>Te beschermen belangen</h1>
<div class="case"><xsl:value-of select="case" /></div>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="edit" />
<xsl:apply-templates select="result" />

<div id="help">
<p>Voer de te beschermen belangen in. Dit betreft alles dat geraakt kan worden indien het proces, waar de informatie binnen de scope ondersteunend aan is, verstoord wordt. Denk bijvoorbeeld aan te behalen organisatiedoelen en imago, maar ook aan belangen van derden.</p>
</div>
</xsl:template>

</xsl:stylesheet>
