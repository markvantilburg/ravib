<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  Licensed under the RAVIB license.
//
//-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Matrix template
//
//-->
<xsl:template match="matrix">
<p>De door RAVIB gehanteerde risicomatrix is als volgt:</p>
<table class="matrix">
<tr><td></td><td></td><td colspan="{count(row)-1}">Impact</td></tr>
<xsl:for-each select="row">
<tr>
	<xsl:if test="position()=1"><td></td></xsl:if>
	<xsl:if test="position()=2"><td rowspan="{count(../row)-1}" class="chance">Kans</td></xsl:if>
	<xsl:for-each select="cell">
		<td><xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if><xsl:value-of select="." /></td>
	</xsl:for-each>
</tr>
</xsl:for-each>
</table>

<p>RAVIB kent de volgende manieren om met een risico om te gaan:</p>
<ul>
<li>Beheersen: Het nemen van maatregelen om de impact van en de kans op een incident te verkleinen.</li>
<li>Ontwijken: Het nemen van maatregelen om de kans op een incident te verkleinen.</li>
<li>Verweren: Het nemen van maatregelen om de impact van een incident te verkleinen.</li>
<li>Accepteren: Het accepteren van de gevolgen indien een incident zich voordoet.</li>
</ul>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Risicomatrix</h1>
<xsl:apply-templates select="matrix" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
