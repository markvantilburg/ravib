<?xml version="1.0" ?>
<!--
//
//  Copyright (c) by Hugo Leisink <hugo@leisink.net>
//  This file is part of the Banshee PHP framework
//  https://www.banshee-php.org/
//
//  Licensed under The MIT License
//
//-->
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:import href="banshee/main.xslt" />

<!--
//
//  request form template
//
//-->
<xsl:template match="request">
<p>Voer uw gebruikersnaam en e-mailadres in op uw wachtwoord te herstellen.</p>
<form action="/{/output/page}" method="post">
<label for="username">Gebruikersnaam:</label>
<input type="text" id="username" name="username" class="form-control" />
<label for="email">E-mailadres:</label>
<input type="text" id="email" name="email" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Herstel wachtwoord" class="btn btn-default" />
<a href="/{@previous}" class="btn btn-default">Cancel</a>
</div>
</form>
</xsl:template>

<!--
//
//  Link sent template
//
//-->
<xsl:template match="link_sent">
<p>Indien u een geldige gebruikersnaam en e-mailadres heeft invoerd, wordt een link om uw wachtwoord te herstellen per e-mail toegestuurd.</p>
<p>Sluit uw browser niet af!!</p>
</xsl:template>

<!--
//
//  Reset form template
//
//-->
<xsl:template match="reset">
<p>Voer een nieuw wachtwoord in voor uw account.:</p>
<xsl:call-template name="show_messages" />
<form action="/{/output/page}" method="post">
<input type="hidden" name="key" value="{key}" />
<input type="hidden" id="username" value="{username}" />
<input type="hidden" id="password_hashed" name="password_hashed" value="no" />
<label for="password">Wachtwoord:</label>
<input type="password" id="password" name="password" class="form-control" />
<label for="repeat">Herhaal wachtwoord:</label>
<input type="password" id="repeat" name="repeat" class="form-control" />

<div class="btn-group">
<input type="submit" name="submit_button" value="Wachtwoord opslaan" class="btn btn-default" />
</div>
</form>
</xsl:template>

<!--
//
//  Crypto template
//
//-->
<xsl:template match="crypto">
<p>Deze RAVIB installatie maakt gebruik van versleuteling om de opgeslagen informatie te beschermen. U wachtwoord is daarbij de sleutel. Voor het behoud van de versteutelde data is bij een wachtwoordwijziging uw huidige wachtwoord nodig. Het herstellen van het wachtwoord is daarom niet mogelijk.</p>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Wachtwoord herstellen</h1>
<xsl:apply-templates select="request" />
<xsl:apply-templates select="link_sent" />
<xsl:apply-templates select="reset" />
<xsl:apply-templates select="crypto" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
