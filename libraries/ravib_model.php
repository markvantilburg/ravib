<?php
	abstract class ravib_model extends Banshee\model {
		private $aes = null;
		protected $risk_matrix = null;
		protected $risk_matrix_labels = null;
		protected $risk_matrix_chance = null;
		protected $risk_matrix_impact = null;
		protected $threat_handle_labels = null;
		protected $availability_score = null;
		protected $integrity_score = null;
		protected $confidentiality_score = null;
		protected $asset_value = null;
		protected $asset_value_labels = null;
		protected $organisation_id = null;

		/* Constructor
		 *
		 * INPUT: object database, object settings, object user, object page, object output[, object language]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct() {
			$arguments = func_get_args();
			call_user_func_array(array("parent", "__construct"), $arguments);

			$cookie = new \Banshee\secure_cookie($this->settings);
			if (isset($_SESSION["advisor_organisation_id"])) {
				if ($this->valid_advisor_id($_SESSION["advisor_organisation_id"])) {
					$this->aes = new \Banshee\Protocol\AES256($cookie->advisor_crypto_key);
					$this->organisation_id = $_SESSION["advisor_organisation_id"];
				} else {
					$this->borrow("adviseur")->deactivate_role();
				}
			}

			if (($this->organisation_id == null) && ($this->user != null)) {
				$this->aes = new \Banshee\Protocol\AES256($cookie->crypto_key);
				$this->organisation_id = $this->user->organisation_id;
			}

			$this->risk_matrix = config_array(RISK_MATRIX);
			foreach ($this->risk_matrix as $i => $row) {
				$this->risk_matrix[$i] = explode(",", $row);
			}

			$this->risk_matrix_labels = config_array(RISK_MATRIX_LABELS);
			$this->risk_matrix_chance = config_array(RISK_MATRIX_CHANCE);
			$this->risk_matrix_impact = config_array(RISK_MATRIX_IMPACT);

			$this->threat_handle_labels = config_array(THREAT_HANDLE_LABELS);

			$this->availability_score = config_array(AVAILABILITY_SCORE);
			$this->integrity_score = config_array(INTEGRITY_SCORE);
			$this->confidentiality_score = config_array(CONFIDENTIALITY_SCORE);

			$this->asset_value = array();
			foreach (config_array(ASSET_VALUE) as $i => $line) {
				$layer = explode(" - ", $line);
				$this->asset_value[$i] = array();
				foreach ($layer as $j => $row) {
					$this->asset_value[$i][$j] = explode(",", $row);
				}
			}
			$this->asset_value_labels = config_array(ASSET_VALUE_LABELS);
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			switch ($key) {
				case "risk_matrix": return $this->risk_matrix;
				case "risk_matrix_labels": return $this->risk_matrix_labels;
				case "risk_matrix_chance": return $this->risk_matrix_chance;
				case "risk_matrix_impact": return $this->risk_matrix_impact;
				case "threat_handle_labels": return $this->threat_handle_labels;
				case "availability_score": return $this->availability_score;
				case "integrity_score": return $this->integrity_score;
				case "confidentiality_score": return $this->confidentiality_score;
				case "asset_value_labels": return $this->asset_value_labels;
				case "organisation_id": return $this->organisation_id;
			}

			return null;
		}

		private function valid_advisor_id($organisation_id) {
			$query = "select count(*) as count from advisors where user_id=%d and organisation_id=%d";
			if (($result = $this->db->execute($query, $this->user->id, $organisation_id)) == false) {
				return false;
			}

			return $result[0]["count"] > 0;
		}

		public function get_case($case_id) {
			$query = "select * from cases where id=%d and organisation_id=%d limit 1";

			if (($result = $this->db->execute($query, $case_id, $this->organisation_id)) == false) {
				return false;
			}

			$this->decrypt($result[0], "name", "organisation", "scope", "impact", "interests");

			return $result[0];
		}

		public function get_standard($standard_id) {
			if (($standard = $this->db->entry("standards", $standard_id)) == false) {
				return false;
			}

			return $standard["name"];
		}

		public function get_organisation($id) {
			if (($organisation = $this->db->entry("organisations", $id)) == false) {
				return false;
			}

			return $organisation["name"];
		}

		public function encrypt(&$data) {
			if (is_false(ENCRYPT_DATA)) {
				return;
			}

			if (func_num_args() > 1) {
				$keys = func_get_args();
				array_shift($keys);
				$keys = array_flatten($keys);
			} else {
				$keys = array_keys($data);
			}

			foreach ($keys as $key) {
				if ($data[$key] != "") {
					$data[$key] = $this->aes->encrypt($data[$key]);
				}
			}
		}

		public function decrypt(&$data) {
			if (is_false(ENCRYPT_DATA)) {
				return;
			}

			if (func_num_args() > 1) {
				$keys = func_get_args();
				array_shift($keys);
				$keys = array_flatten($keys);
			} else {
				$keys = array_keys($data);
			}

			foreach ($keys as $key) {
				if (($data[$key] ?? null) != "") {
					$data[$key] = $this->aes->decrypt($data[$key]);
				}
			}
		}
	}
?>
