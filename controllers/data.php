<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class data_controller extends ravib_controller {
		protected $prevent_repost = false;

		private function show_overview() {	
			if (($organisation = $this->model->get_organisation($this->model->organisation_id)) == false) {
				$this->view->add_tag("result", "Database error");
				return false;
			}

			$this->view->open_tag("overview");
			if (isset($_SESSION["advisor_organisation_id"]) != false) {
				$this->view->add_tag("organisation", $organisation);
			}
			$this->view->close_tag();
		}

		private function export_data() {
			if (($export = $this->model->get_export()) == false) {
				$this->view->add_tag("result", "Export error.");
				return false;
			}

			$export["signature"] = $this->model->signature($export);

			if (($organisation = $this->model->get_organisation($this->model->organisation_id)) == false) {
				$organisation = "Backup";
			}

			$this->view->disable();

			$filename = $this->model->generate_filename($organisation)." ".date("Y-m-d").".ravib";
			header("Content-Type: application/x-binary");
			header("Content-Disposition: attachment; filename=\"".$filename."\"");
			print gzencode(json_encode($export));

			return true;
		}

		private function import_data($data) {
			if (substr($data, 0, 2) != "\x1F\x8B") {
				$this->view->add_system_warning("Bestand bevat geen RAVIB data.");
				return false;
			}

			if (($data = @gzdecode($data)) === false) {
				$this->view->add_system_warning("Fout bij het decomprimeren van de RAVIB data.");
				return false;
			}

			if (($data = @json_decode($data, true)) === null) {
				$this->view->add_system_warning("Fout bij het inlezen van de RAVIB data.");
				return false;
			}

			$signature = $data["signature"];
			unset($data["signature"]);

			if ($this->settings->validate_import_signature) {
				if ($this->model->signature($data) != $signature) {
					$this->view->add_system_warning("De handtekening van de RAVIB data is niet geaccepteerd.");
					return false;
				}
			}

			return $this->model->import_data($data);
		}

		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Exporteren") {
					$this->export_data();
				} else if ($_POST["submit_button"] == "Importeren") {
					if ($_FILES["file"]["error"] == 0) {
						$data = file_get_contents($_FILES["file"]["tmp_name"]);
						if ($this->import_data($data) == false) {
							$this->view->add_system_warning("Fout tijdens importeren van het bestand.");
						} else {
							$this->view->add_system_message("Bestand is geïmporteerd.");
						}
					}
				}
			}

			$this->show_overview();
		}
	}
?>
