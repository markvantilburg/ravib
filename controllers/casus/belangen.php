<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_belangen_controller extends ravib_controller {
		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			$this->show_breadcrumbs($case_id);

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->save_interests($_POST["interests"], $this->case["id"]) == false) {
					$this->view->add_message("Fout bij het opslaan.");
				} else {
					$this->case["interests"] = $_POST["interests"];
				}
			}
			
			if (($this->page->parameters[1] ?? null) == "handout") {
				if (($handout = $this->model->make_handout($this->case)) === false) {
					$this->view->add_tag("result", "Fout bij het maken van de handout.");
				} else {
					$this->view->disable();
					$case_name = $this->generate_filename($this->case["organisation"]." - ".$this->case["name"]);
					$handout->Output($case_name." - handout.pdf", "I");
				}
			} else if (($this->page->parameters[1] ?? null) == "edit") {
				$this->view->add_tag("edit", $this->case["interests"]);
			} else {
				$this->view->add_tag("overview", $this->case["interests"]);
			}
		}
	}
?>
