<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_voortgang_rapportage_controller extends ravib_controller {
		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			if (($report = $this->model->generate_report($this->case)) === false) {
				$this->view->add_tag("result", "Fout bij genereren rapportage.");
			} else {
				$this->view->disable();
				$case_name = $this->generate_filename($this->case["organisation"]." - ".$this->case["name"]);
				$report->Output($case_name." - voortgang.pdf", "I");
			}
		}
	}
?>
