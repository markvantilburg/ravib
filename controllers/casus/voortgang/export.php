<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_voortgang_export_controller extends ravib_controller {
		public function execute() {
			$case_id = $this->page->parameters[0] ?? null;
			if ($this->valid_case_id($case_id) == false) {
				return;
			}

			if (($csv = $this->model->get_export_csv($this->case)) == false) {
				$this->view->add_tag("result", "Fout bij het genereren van de export.");
			} else {
				$case_name = $this->generate_filename($this->case["organisation"]." - ".$this->case["name"]);
				print $csv->to_output($this->view, $case_name." - voortgang");
			}
		}
	}
?>
