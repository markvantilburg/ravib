<?php
	class casus_risico_controller extends \Banshee\controller {
		protected $prevent_repost = false;

		private function show_matrix($access_code) {
			$risk_matrix = config_array(RISK_MATRIX);
			$risk_matrix_chance = config_array(RISK_MATRIX_CHANCE);
			$risk_matrix_impact = config_array(RISK_MATRIX_IMPACT);

			$this->view->open_tag("matrix");

			$this->view->open_tag("row");
			$this->view->add_tag("cell", "");
			foreach ($risk_matrix_impact as $impact) {
				$this->view->add_tag("cell", $impact);
			}
			$this->view->close_tag();

			$matrix_votes = array(
				array(0, 0, 0, 0, 0),
				array(0, 0, 0, 0, 0),
				array(0, 0, 0, 0, 0),
				array(0, 0, 0, 0, 0),
				array(0, 0, 0, 0, 0));

			if (($votes = $this->model->get_votes($access_code)) != false) {
				foreach ($votes as $vote) {
					$matrix_votes[$vote["chance"] - 1][$vote["impact"] - 1]++;
				}
			}

			$chance = 4;
			foreach (array_reverse($risk_matrix) as $y => $row) {
				$row = explode(",", $row);

				$this->view->open_tag("row");
				$this->view->add_tag("cell", $risk_matrix_chance[$chance--]);
				foreach ($row as $x => $cell) {
					if (($value = $matrix_votes[4 - $y][$x]) == 0) {
						$value = "";
					}
					$this->view->add_tag("cell", $value, array("class" => "risk risk_".$cell));
				}
				$this->view->close_tag();
			}

			$this->view->close_tag();
		}

		public function execute() {
			$this->view->title = "Risicomatrix";

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Nieuwe toegangscode") {
					$this->model->delete_session($_SESSION["risk_assess_access_code"]);
					$_SESSION["risk_assess_access_code"] = null;
				}
			}

			if (($_SESSION["risk_assess_access_code"] ?? null) != null) {
				if ($this->model->session_exists($_SESSION["risk_assess_access_code"]) == false) {
					$_SESSION["risk_assess_access_code"] = null;
				}
			}

			if (($_SESSION["risk_assess_access_code"] ?? null) == null) {
				$_SESSION["risk_assess_access_code"] = random_int(100000, 999999);
				if ($this->model->create_session($_SESSION["risk_assess_access_code"]) == false) {
					$this->view->add_tag("result", "Fout bij aanmaken sessie.");
					return;
				}
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Leeg matrix") {
					$this->model->flush_votes($_SESSION["risk_assess_access_code"]);
				}
			}

			$this->view->add_help_button();

			$this->view->open_tag("assess");
			$this->view->add_tag("access_code", $_SESSION["risk_assess_access_code"]);
			$this->show_matrix($_SESSION["risk_assess_access_code"]);
			$this->view->close_tag();
		}
	}
?>
