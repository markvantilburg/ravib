function highlight(option) {
	$('table.measures td').css('background-color', '#ffffff');

	var threat_id = $(option).val();
	if (threat_id > 0) {
		$.get('/casus/maatregelen/'+threat_id, function(data) {
			$(data).find('measure').each(function() {
				var measure_id = $(this).text();
				$('table.measures tr.measure_'+measure_id+' td').css('background-color', '#ffffd0');
			});
		});
	}
}
