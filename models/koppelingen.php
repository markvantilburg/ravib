<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class koppelingen_model extends Banshee\model {
		public function get_standards() {
			$query = "select * from standards order by id desc";

			return $this->db->execute($query);
		}

		public function get_threat_categories() {
			$query = "select * from threat_categories";
			if (($categories = $this->db->execute($query)) == false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[$category["id"]] = $category["name"];
			}

			return $result;
		}

		public function get_threats() {
			$query = "select * from threats order by number";
			if (($threats = $this->db->execute($query)) === false) {
				return false;
			}

			$result = array();
			foreach ($threats as $threat) {
				foreach (array("availability", "integrity", "confidentiality") as $key) {
					if ($threat[$key] == "") {
						$threat[$key] = "-";
					}
				}
				$result[$threat["id"]] = $threat;
			}

			return $result;
		}

		public function get_measures($standard_id) {
			$query = "select * from measures where standard_id=%d";
			if (($measures = $this->db->execute($query, $standard_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($measures as $measure) {
				$result[$measure["id"]] = $measure;
			}

			return $result;
		}

		public function get_measure_categories($standard_id) {
			$query = "select * from measure_categories where standard_id=%d order by number";

			if (($categories = $this->db->execute($query, $standard_id)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["number"]] = $category;
			}

			return $result;
		}

		public function get_mitigation($standard_id) {
			$query = "select c.* from mitigation c, measures i ".
			         "where c.measure_id=i.id and i.standard_id=%d";

			return $this->db->execute($query, $standard_id);
		}
	}
?>
