<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class adviseur_model extends Banshee\model {
		public function get_roles() {
			$query = "select a.id, o.name, a.crypto_key, o.id as organisation_id ".
			         "from advisors a, organisations o ".
			         "where a.organisation_id=o.id and a.user_id=%d order by name";

			return $this->db->execute($query, $this->user->id);
		}

		public function request_adviser_role($user) {
			$query = "select o.id from organisations o, users u ".
			         "where o.id=u.organisation_id and (u.username=%s or u.email=%s) and o.id!=%d";
			if (($result = $this->db->execute($query, $user, $user, $this->user->organisation_id)) == false) {
				$this->view->add_message("Gebruiker niet gevonden.");
				return false;
			}
			$organisation_id = $result[0]["id"];

			$query = "select count(*) as count from advisors where organisation_id=%d and user_id=%d";
			if (($result = $this->db->execute($query, $organisation_id, $this->user->id)) == false) {
				$this->view->add_message("Database error.");
				return false;
			}
			if ($result[0]["count"] > 0) {
				$this->view->add_message("U treedt al op als adviseur voor die organisatie.");
				return false;
			}

			$rsa = new \Banshee\Protocol\RSA(2048);
			$cookie = new \Banshee\secure_cookie($this->settings);
			$aes = new \Banshee\Protocol\AES256($cookie->crypto_key);
			$private_key = $aes->encrypt($rsa->private_key);

			$data = array(
				"id"              => null,
				"organisation_id" => $organisation_id,
				"user_id"         => $this->user->id,
				"private_key"     => $private_key,
				"public_key"      => $rsa->public_key,
				"crypto_key"      => null);

			return $this->db->insert("advisors", $data) !== false;
		}

		public function activate_role($id) {
			$query = "select * from advisors where id=%d and user_id=%d";
			if (($result = $this->db->execute($query, $id, $this->user->id)) === false) {
				return false;
			}
			$advisor = $result[0];

			$cookie = new \Banshee\secure_cookie($this->settings);
			$aes = new \Banshee\Protocol\AES256($cookie->crypto_key);
			$private_key = $aes->decrypt($advisor["private_key"]);

			$rsa = new \Banshee\Protocol\RSA($private_key);
			$crypto_key = $rsa->decrypt_with_private_key($advisor["crypto_key"]);

			$_SESSION["advisor_organisation_id"] = (int)$advisor["organisation_id"];
			$cookie->advisor_crypto_key = $crypto_key;

			$this->user->override_role($this->settings->advisor_role);

			$this->user->log_action("advisor role activated for ".$this->get_organisation($advisor["organisation_id"]));

			return true;
		}

		public function get_organisation($id) {
			if (($organisation = $this->db->entry("organisations", $id)) == false) {
				return false;
			}

			return $organisation["name"];
		}

		public function deactivate_role() {
			unset($_SESSION["advisor_organisation_id"]);
			$cookie = new \Banshee\secure_cookie($this->settings);
			$cookie->advisor_crypto_key = null;

			$this->user->log_action("advisor role deactivated");
		}

		public function delete_role($id) {
			$query = "delete from advisors where id=%d and user_id=%d";

			return $this->db->query($query, $id, $this->user->id) !== false;
		}
	}
?>
