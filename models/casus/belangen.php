<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class casus_belangen_model extends ravib_model {
		public function save_interests($interests, $case_id) {
			$data = array("interests" => $interests);

			$this->encrypt($data, "interests");

			return $this->db->update("cases", $case_id, $data) !== false;
		}

		public function add_bia(&$pdf, $bia_items) {
			foreach ($bia_items as $nr => $item) {
				if ($item["scope"] == NO) {
					continue;
				}

				$pdf->SetFillColor(232, 232, 232);
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(0, 5, $item["item"], 0, 0, "", true);
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Ln(5);

				if ($item["description"] != "") {
					$pdf->SetLeftMargin(19);
					$pdf->Write(5, $item["description"]);
					$pdf->SetLeftMargin(15);
					$pdf->Ln(5);
				}
				$pdf->SetFont("helvetica", "", 9);
				$pdf->Cell(4, 5);
				$pdf->Cell(24, 5, "B: ".($this->availability_score[$item["availability"] - 1] ?? ""));
				$pdf->Cell(23, 5, "I: ".($this->integrity_score[$item["integrity"] - 1] ?? ""));
				$pdf->Cell(28, 5, "V: ".($this->confidentiality_score[$item["confidentiality"] - 1] ?? ""));
				$pdf->Cell(35, 5, "Waarde: ".$this->asset_value_labels[$item["value"]]);
				$pdf->Cell(17, 5, "Pg: ".(is_true($item["personal_data"]) ? "ja" : "nee"));
				$pdf->Cell(25, 5, "Eigenaar: ".(is_true($item["owner"]) ? "ja" : "nee"));
				$pdf->Cell(0, 5, "Locatie: ".$item["location"]);
				$pdf->Ln(5);
				$pdf->SetFont("helvetica", "", 10);

				$pdf->AddTextBlock("Impact van incident", $item["impact"]);
				$pdf->Ln(1);
			}

			return true;
		}

		public function add_actors(&$pdf, $case_id) {
			if (($actors = $this->borrow("actoren")->get_actors()) === false) {
				return false;
			}

			$pdf->SetFont("helvetica", "B", 11);
			$pdf->Cell(55, 6, "Naam", "B");
			$pdf->Cell(40, 6, "Bereidheid / kans", "B");
			$pdf->Cell(30, 6, "Kennisniveau", "B");
			$pdf->Cell(25, 6, "Middelen", "B");
			$pdf->Cell(30, 6, "Dreiging", "B");
			$pdf->Ln(8);

			$actor_chance = config_array(ACTOR_CHANCE);
			$actor_knowledge = config_array(ACTOR_KNOWLEDGE);
			$actor_resources = config_array(ACTOR_RESOURCES);
			$actor_threat = config_array(ACTOR_THREAT_LABELS);

			$pdf->SetFont("helvetica", "", 10);
			foreach ($actors as $actor) {
				$pdf->Cell(55, 5, $actor["name"]);
				$pdf->Cell(40, 5, $actor_chance[$actor["chance"] - 1]);
				$pdf->Cell(30, 5, $actor_knowledge[$actor["knowledge"] - 1]);
				$pdf->Cell(25, 5, $actor_resources[$actor["resources"] - 1]);
				$threat_level = $this->borrow("actoren")->actor_threat($actor);
				$pdf->Cell(30, 5, $actor_threat[$threat_level]);
				$pdf->Ln(5);
				if (trim($actor["reason"]) != "") {
					$pdf->Cell(5, 5, "");
					$pdf->Cell(13, 5, "Reden: ");
					$pdf->Cell(162, 5, $actor["reason"]);
					$pdf->Ln(5);
				}
			}
			$pdf->Ln(10);

			return true;
		}

		private function capitalize_first_letter($word) {
			$word[0] = strtoupper($word[0]);

			return $word;
		}

		public function make_handout($case) {
			if (($bia_items = $this->borrow("casus/scope")->get_scope($case["id"])) === false) {
				return false;
			}

			if (($templates = $this->borrow("casus/dreigingen")->get_threat_templates()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			if (($categories = $this->borrow("casus/dreigingen")->get_categories()) == false) {
				$this->view->add_tag("result", "Database error.");
				return;
			}

			$pdf = new RAVIB_report($case["title"], true);
			$pdf->SetAuthor($this->get_organisation($this->user->organisation_id)." en RAVIB");
			$pdf->SetSubject("Overzicht belangen, scope en actoren");
			$pdf->SetKeywords("RAVIB, handout, Business Impact Analyse, actoren");
			$pdf->AliasNbPages();

			/* Information systems
			 */
			$pdf->AddPage();
			$pdf->SetFont("helvetica", "B", 15);
			$pdf->Cell(0, 5, "Handout voor ".$case["name"], 0, 1, "C");
			if ($case["interests"] != "") {
				$pdf->Ln(8);
				$pdf->AddChapter("Te beschermen belangen");
				$pdf->Write(5, $case["interests"]);
			}
			$pdf->Ln(12);

			$pdf->AddChapter("Overzicht informatiesystemen");
			$pdf->Ln(3);

			$this->add_bia($pdf, $bia_items);

			/* Actors
			 */
			$pdf->AddPage();
			$pdf->AddChapter("Actoren");

			$this->add_actors($pdf, $case["id"]);

			/* Chance
			 */
			$actor_threat = config_array(ACTOR_THREAT_LABELS);
			$frequency = config_array(CHANCE_FREQUENCY);
			$pdf->AddChapter("Bepalen van de kans");
			$pdf->Write(5, "Bij beperkte maatregelen tegen een actor mag de kans een positie worden verlaagd. Bij voldoende maatregelen tegen een actor mag de kans twee posities worden verlaagd.");
			$pdf->Ln(7);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "", "B");
			$pdf->Cell(45, 5, "Dreiging vanuit actor", "B");
			$pdf->Cell(100, 5, "Frequentie onbedoeld incident", "B");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "", 10);
			for ($i = count($this->risk_matrix_chance) - 1; $i>= 0; $i--) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(35, 5, $this->capitalize_first_letter($this->risk_matrix_chance[$i]).": ");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Cell(45, 5, $actor_threat[$i]);
				$pdf->Cell(100, 5, $frequency[$i]);
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Impact values
			 */
			$pdf->AddChapter("Invulling van de impact");
			$impact_values = json_decode($case["impact"], true);
			for ($i = count($this->risk_matrix_impact) - 1; $i>= 0; $i--) {
				$pdf->SetFont("helvetica", "B", 10);
				$pdf->Cell(35, 5, $this->capitalize_first_letter($this->risk_matrix_impact[$i]).": ");
				$pdf->SetFont("helvetica", "", 10);
				$pdf->Write(5, $impact_values[$i]);
				$pdf->Ln(5);
			}
			$pdf->Ln(10);

			/* Action
			 */
			$pdf->AddChapter("Betekenis van de aanpak");
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Beheersen:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het nemen van maatregelen om de impact van en de kans op een incident te verkleinen.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Ontwijken:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het nemen van maatregelen om de kans op een incident te verkleinen.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Verweren:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het nemen van maatregelen om de impact van een incident te verkleinen.");
			$pdf->Ln(5);
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Accepteren:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->Write(5, "Het accepteren van de gevolgen indien een incident zich voordoet.");
			$pdf->Ln(15);

			/* Scales
			 */
			$pdf->AddChapter("Schalen bij BIA en Actoren");
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Beschikbaarheid:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(AVAILABILITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Integriteit:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(INTEGRITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Vertrouwelijkheid:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(CONFIDENTIALITY_SCORE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Waarde:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ASSET_VALUE_LABELS)));

			$pdf->Ln(3);

			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Bereidheid / kans:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ACTOR_CHANCE)));
			$pdf->SetFont("helvetica", "B", 10);
			$pdf->Cell(35, 5, "Kennisniveau:");
			$pdf->SetFont("helvetica", "", 10);
			$pdf->MultiCell(140, 5, implode(", ", config_array(ACTOR_KNOWLEDGE)));

			/* Threats
			 */
			$pdf->AddPage();
			$pdf->AddChapter("Mogelijke dreigingen");
			$pdf->Ln(2);

			$category_id = 0;
			foreach ($templates as $template) {
				if ($template["category_id"] != $category_id) {
					$category_id = $template["category_id"];
					$pdf->Ln(1);
					$pdf->SetFillColor(232, 232, 232);
					$pdf->SetFont("helvetica", "B", 10);
					$pdf->Cell(0, 5, $categories[$category_id]["name"], 0, 0, "", true);
					$pdf->SetFont("helvetica", "", 10);
					$pdf->Ln(5);
				}
				$pdf->Cell(7, 5, $template["number"].".");
				$pdf->Cell(0, 5, $template["threat"]);
				$pdf->Ln(5);
				$pdf->SetLeftMargin(22);
				$pdf->SetFont("helvetica", "I", 9);
				$pdf->Write(4, $template["description"]);
				$pdf->SetFont("helvetica", "", 10);
				$pdf->SetLeftMargin(15);
				$pdf->Ln(4);
			}

			return $pdf;
		}
	}
?>
