<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_measures_categories_model extends Banshee\tablemanager_model {
		protected $table = "measure_categories";
		protected $order = "number";
		protected $elements = array(	
			"standard_id" => array(
				"label"    => "Standard",
				"type"     => "integer",
				"overview" => false,
				"required" => true,
				"readonly" => true),
			"number" => array(
				"label"    => "Number",
				"type"     => "integer",
				"overview" => true,
				"required" => true),
			"name" => array(
				"label"    => "Name",
				"type"     => "varchar",
				"overview" => true,
				"required" => true));

		public function get_standard($standard) {
			return $this->borrow("cms/standards")->get_item($standard);
		}

		public function get_items() {
			$query = "select * from %S where standard_id=%d order by %S";

			return $this->db->execute($query, $this->table, $_SESSION["standard"], $this->order);
		}

		public function create_item($item) {
			$item["standard_id"] = $_SESSION["standard"];

			parent::create_item($item);
		}
	}
?>
