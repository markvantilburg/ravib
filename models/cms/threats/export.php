<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_threats_export_model extends Banshee\model {
		public function get_categories() {
			$query = "select * from threat_categories order by name";

			if (($categories = $this->db->execute($query)) === false) {
				return false;
			}

			$result = array();
			foreach ($categories as $category) {
				$result[(int)$category["id"]] = $category;
			}

			return $result;
		}

		public function get_threats($standard) {
			$query = "select t.*,(select count(*) from mitigation g, measures m ".
					 "where g.measure_id=m.id and standard_id=%d and threat_id=t.id) as links from threats t ".
					 "order by number";

			return $this->db->execute($query, $standard);
		}

		public function get_mitigation($threat_id) {
			$query = "select m.number from measures m, mitigation g ".
			         "where m.id=g.measure_id and g.threat_id=%d";

			return $this->db->execute($query, $threat_id);
		}
	}
?>
