<?php
	/* Copyright (c) by Hugo Leisink <hugo@leisink.net>
	 * Licensed under the RAVIB license.
	 */

	class cms_validate_model extends Banshee\model {
		public function get_standard($standard) {
			return $this->borrow("cms/standards")->get_item($standard);
		}

		public function linked_threats($standard) {
			$query = "select *,(select count(*) from mitigation g, measures m ".
			                   "where threat_id=t.id and g.measure_id=m.id and m.standard_id=%d) as links ".
			         "from threats t order by links,number";
			if (($threats = $this->db->execute($query, $standard)) === false) {
				return false;
			}

			return $threats;
		}

		public function linked_measures($standard) {
			$query = "select *,(select count(*) from mitigation where measure_id=m.id) as links ".
			         "from measures m where standard_id=%d order by links,id";
			if (($measures = $this->db->execute($query, $standard)) === false) {
				return false;
			}

			return $measures;
		}
	}
?>
