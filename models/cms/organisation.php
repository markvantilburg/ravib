<?php
	class cms_organisation_model extends Banshee\model {
		private $columns = array("name", "last_login");

		public function count_organisations() {
			$query = "select count(*) as count from organisations";

			if (($result = $this->db->execute($query)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_organisations($offset, $limit) {
			if (isset($_SESSION["organisation_order"]) == false) {
				$_SESSION["organisation_order"] = $this->columns;
			}

			if (isset($_GET["order"])) {
				if ((in_array($_GET["order"], $this->columns)) && ($_GET["order"] != $_SESSION["organisation_order"][0])) {
					$_SESSION["organisation_order"] = array($_GET["order"], $_SESSION["organisation_order"][0]);
				}
			}

			$query = "select id, name, UNIX_TIMESTAMP(last_login) as last_login, ".
					 "(select count(*) from users where organisation_id=o.id) as users ".
			         "from organisations o order by %S,%S limit %d,%d";

			return $this->db->execute($query, $_SESSION["organisation_order"], $offset, $limit);
		}

		public function get_organisation($organisation_id) {
			return $this->db->entry("organisations", $organisation_id);
		}

		public function get_users($organisation_id) {
			$query = "select * from users where organisation_id=%d order by fullname";

			return $this->db->execute($query, $organisation_id);
		}

		public function save_oke($organisation) {
			$result = true;

			if (trim($organisation["name"]) == "") {
				$this->view->add_message("Empty name is not allowed.");
				$result = false;
			}

			if (($check = $this->db->entry("organisations", $organisation["name"], "name")) === false) {
				$this->view->add_message("Database error.");
				$result = false;
			} else if ($check != false) {
				if ($check["id"] != ($organisation["id"] ?? null)) {
					$this->view->add_message("Organisation name already exists.");
					$result = false;
				}
			}

			return $result;
		}

		public function create_organisation($organisation) {
			$keys = array("id", "name");

			$organisation["id"] = null;

			return $this->db->insert("organisations", $organisation, $keys);
		}

		public function update_organisation($organisation) {
			$keys = array("name");

			return $this->db->update("organisations", $organisation["id"], $organisation, $keys);
		}

		public function delete_oke($organisation_id) {
			if ($organisation_id == $this->user->organisation_id) {
				$this->view->add_system_warning("You can't delete your own organisation.");
				return false;
			}

			return true;
		}

		public function delete_organisation($organisation_id) {
			$this->db->query("begin");

			/* Delete cases
			 */
			$query = "select id from cases where organisation_id=%d";
			if (($cases = $this->db->execute($query, $organisation_id)) === false) {
				$this->db->query("rollback");
				return false;
			}

			foreach ($cases as $case) {
				if ($this->borrow("casus")->delete_case($case["id"]) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			/* Delete advisors, BIA and actors
			 */
			$tables = array("advisors", "bia", "actors");
			foreach ($tables as $table) {
				$query = "delete from %S where organisation_id=%d";
				if ($this->db->query($query, $table, $organisation_id) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			/* Delete users
			 */
			if (($users = $this->get_users($organisation_id)) === false) {
				$this->db->query("rollback");
				return false;
			}

			foreach ($users as $user) {
				if ($this->borrow("cms/user")->delete_user($user["id"]) === false) {
					$this->db->query("rollback");
					return false;
				}
			}

			/* Delete organisation
			 */
			if ($this->db->delete("organisations", $organisation_id) === false) {
				$this->db->query("rollback");
				return false;
			}

			return $this->db->query("commit") !== false;
		}
	}
?>
